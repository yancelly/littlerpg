# LittleRPG

## Comment exécuter le programme ?

Sous Windows :

1. Télécharger l'exécutable ici : https://gitlab.com/yancelly/littlerpg/blob/master/LittleRPG.exe
2. Lancer le jeu
3. Le message de prévention est un faux positif, le code source est disponible pour vérifier.

Sous Linux :

1. Télécharger le fichier jar ici : https://gitlab.com/yancelly/littlerpg/blob/master/LittleRPG.jar
2. Ouvrir un terminal à l'emplacement du fichier
3. Taper la commande : `` java -jar LittleRPG.jar ``