/* LittleRPG - Jeu d'aventure où le joueur doit monter des étages.
 * 
 * Copyright (C) 2019  Yoan ANCELLY

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. 

 */

/* La classe Arme permet de créer des armes pour les personnage. 
 * 
 * Différentes qualités d'Arme sont disponibles : Commun, Rare, Épique, Légendaire
 * Les caractéristiques changent et donnent des bonus spéciaux selon l'objet
 * 
 * On trouve également la liste du nom des Arme dans chaque catégorie
 * 
 * */

package jeux;

public class Arme {

	// ----------- Attributs -----------
	private int degats, degatsMin, degatsMax, rangeDegats;
	private String nomArme, nomAttribut, nomAttribut1, nomAttribut2, descriptionAttribut, descriptionAttribut1, descriptionAttribut2;
	private boolean maudit, poison, doubleTranchant, etourdissement, sanguinaire;
	private boolean estLegendaire;

	// ----------- Constructeurs -----------
	public Arme() {
		nomArme = "Arme d'entraînement";
		degatsMax = 5;
		degatsMin = 3;
		rangeDegats = (degatsMax - degatsMin) + 1;
		maudit = false;
		doubleTranchant = false;
		poison = false;
		etourdissement = false;
		sanguinaire = false;
		descriptionAttribut = descriptionAttribut1 = descriptionAttribut2 = "Aucun";
		nomAttribut = nomAttribut1 = nomAttribut2 = "";

	}

	// ----------- Accesseurs -----------
	public int getDegats() {
		return degats;
	}

	public int getDegatsMax() {
		return degatsMax;
	}

	public int getDegatsMin() {
		return degatsMin;
	}

	public int getRangeDegats() {
		return rangeDegats;
	}

	public String getNomArme() {
		return nomArme;
	}

	public boolean getMaudit() {
		return maudit;
	}

	public boolean getPoison() {
		return poison;
	}

	public boolean getDoubleTranchant() {
		return doubleTranchant;
	}

	public boolean getEtourdissement() {
		return etourdissement;
	}

	public boolean getSanguinaire() {
		return sanguinaire;
	}

	public boolean getEstLegendaire() {
		return estLegendaire;
	}

	// ----------- Modulateurs -----------
	public void setNomArme(String n) {
		nomArme = n;
	}

	public void setDegatsMax(int d) {
		degatsMax = d;
	}

	public void setDegatsMin(int d) {
		degatsMin = d;
	}

	public void setDegats(int d) {
		degats = d;
	}

	public void setEstLegendaire(boolean etat) {
		estLegendaire = etat;
	}

	// ----------- Méthodes -----------

	// Liste des attributs
	public void choixAttributsArme(Joueur joueur) {
		int nbAttribut = 5;
		int choixAttr1 = 0;
		int choixAttr2 = 0;

		choixAttr1 = (int) (Math.random() * nbAttribut + 1);

		// Attribut 1

		if(choixAttr1 == 1) {
			attrMaudit();
			nomAttribut1 = nomAttribut;
			descriptionAttribut1 = descriptionAttribut;
		}
		if(choixAttr1 == 2) {
			attrPoison();
			nomAttribut1 = nomAttribut;
			descriptionAttribut1 = descriptionAttribut;
		}
		if(choixAttr1 == 3) {
			attrDoubleTranchant();
			nomAttribut1 = nomAttribut;
			descriptionAttribut1 = descriptionAttribut;
		}
		if(choixAttr1 == 4) {
			attrEtourdissement();
			nomAttribut1 = nomAttribut;
			descriptionAttribut1 = descriptionAttribut;
		}
		if(choixAttr1 == 5) {
			attrSanguinaire();
			nomAttribut1 = nomAttribut;
			descriptionAttribut1 = descriptionAttribut;
		}

		// Attribut 2
		
		if(estLegendaire == true) {
			do {
				choixAttr2 = (int) (Math.random() * nbAttribut + 1);
			} while (choixAttr1 == choixAttr2);

			if(choixAttr2 == 1) {
				attrMaudit();
				nomAttribut2 = nomAttribut;
				descriptionAttribut2 = descriptionAttribut;
			}
			if(choixAttr2 == 2) {
				attrPoison();
				nomAttribut2 = nomAttribut;
				descriptionAttribut2 = descriptionAttribut;
			}
			if(choixAttr2 == 3) {
				attrDoubleTranchant();
				nomAttribut2 = nomAttribut;
				descriptionAttribut2 = descriptionAttribut;
			}
			if(choixAttr2 == 4) {
				attrEtourdissement();
				nomAttribut2 = nomAttribut;
				descriptionAttribut2 = descriptionAttribut;
			}
			if(choixAttr2 == 5) {
				attrSanguinaire();
				nomAttribut2 = nomAttribut;
				descriptionAttribut2 = descriptionAttribut;
			}

		}

	}

	// Reset les attributs
	public void resetAttributs() {
		maudit= poison= doubleTranchant= etourdissement= sanguinaire = false;
		nomAttribut = nomAttribut1 = nomAttribut2 = "";
		descriptionAttribut = descriptionAttribut1 = descriptionAttribut2 = "Aucun";
	}
	
	// Attribut Maudite
	public void attrMaudit() {
		maudit = true;
		nomAttribut = "Maudite";
		descriptionAttribut = "Diminue les dégâts infligés par l'ennemi de 35% et augmente les dégâts infligés de 20% à l'ennemi";
	}
	
	// Attribut Poison
	public void attrPoison() {
		poison = true;
		nomAttribut = "Empoisonnée";
		descriptionAttribut = "Inflige 50% des dégâts maximum de l'arme pendant 2 tours (35% de chance d'activation)";
	}
	
	// Attribut Double Tranchant
	public void attrDoubleTranchant() {
		doubleTranchant = true;
		nomAttribut = "Double Tranchant";
		descriptionAttribut = "Inflige une seconde attaque égale à 33% des dégâts maximum de l'arme";
	}
	
	// Attribut Etourdissement
	public void attrEtourdissement() {
		etourdissement = true;
		nomAttribut = "Étourdissante";
		descriptionAttribut = "Empêche l'ennemi d'attaquer pendant 2 tours (20% de chance d'activation)";
	}
	
	// Attribut Sanguinaire
	public void attrSanguinaire() {
		sanguinaire = true;
		nomAttribut = "Sanguinaire";
		descriptionAttribut = "Redonne 35% des points de santé par coup";
	}

	// Affiche les statistiques de l'Arme
	public void afficherStatsArme() {
		System.out.println(""); // Saut de ligne
		System.out.println("---------------- Stats Arme ----------------");
		System.out.println("Nom : " + nomArme + " " + nomAttribut1 + " " + nomAttribut2);
		System.out.println("Dégâts : " + (degatsMax+degatsMin)/2 + " [" + degatsMin + "-" + degatsMax + "]");
		System.out.println("Attribut 1 : " + descriptionAttribut1);
		System.out.println("Attribut 2 : " + descriptionAttribut2);
		System.out.println("--------------------------------------------");
		System.out.println(""); // Saut de ligne
	}

	// Échanger son arme actuelle contre l'Arme proposée
	public void echangerArme(Arme armeDrop) {
		// Reset les attributs
		resetAttributs();
		// Attributs de l'arme
		nomArme = armeDrop.nomArme;
		degatsMin = armeDrop.degatsMin;
		degatsMax = armeDrop.degatsMax;
		nomAttribut1 = armeDrop.nomAttribut1;
		nomAttribut2 = armeDrop.nomAttribut2;
		descriptionAttribut1 = armeDrop.descriptionAttribut1;
		descriptionAttribut2 = armeDrop.descriptionAttribut2;
		// Attributs spéciaux
		maudit = armeDrop.maudit;
		doubleTranchant = armeDrop.doubleTranchant;
		poison = armeDrop.poison;
		etourdissement = armeDrop.etourdissement;
		sanguinaire = armeDrop.sanguinaire;
	}

	// L'arme devient de qualité Commun
	public void armeCommun(Joueur joueur) {
		estLegendaire = false;
		nomArme = "Arme Commun";
		degatsMin = 3 + joueur.getNiveau()*2 - 2;
		degatsMax = 5 + joueur.getNiveau()*2 - 2;	
		descriptionAttribut1 = descriptionAttribut2 = "Aucun";
		nomAttribut1 = nomAttribut2 = "";
	}

	// L'arme devient de qualité Rare
	public void armeRare(Joueur joueur) {
		estLegendaire = false;
		nomArme = "Arme Rare";
		degatsMin = 5 + joueur.getNiveau()*2 - 2;
		degatsMax = 7 + joueur.getNiveau()*2 - 2;
		descriptionAttribut1 = descriptionAttribut2 = "Aucun";
		nomAttribut1 = nomAttribut2 = "";
	}

	// L'arme devient de qualité Épique
	public void armeEpique(Joueur joueur) {
		estLegendaire = false;
		nomArme = "Arme Épique";
		degatsMin = 7 + joueur.getNiveau()*2 - 2;
		degatsMax = 9 + joueur.getNiveau()*2 - 2;
	}

	// L'arme devient de qualité Légendaire
	public void armeLegendaire(Joueur joueur) {
		estLegendaire = true;
		nomArme = "Arme Légendaire";
		degatsMin = 9 + joueur.getNiveau()*2 - 2;
		degatsMax = 11 + joueur.getNiveau()*2 - 2;
	}
}
