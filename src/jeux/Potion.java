/* LittleRPG - Jeu d'aventure où le joueur doit monter des étages.
 * 
 * Copyright (C) 2019  Yoan ANCELLY

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. 
    
*/

package jeux;

public class Potion {

	// ----------- Attributs -----------
	private int nbPotion, valeurPotion, nbPotionLimite;

	// Constructeur
	public Potion() {
		valeurPotion = 10;
		nbPotion = 2;
		nbPotionLimite = 20;
	}

	// ----------- Accesseurs -----------

	public int getNbPotion() {
		return nbPotion;
	}

	public int getValeurPotion() {
		return valeurPotion;
	}
	
	public int getNbPotionLimite() {
		return nbPotionLimite;
	}


	// Modulateurs
	public void setNbPotion(int nb) {
		nbPotion = nb;
	}
	
	public void setValeurPotion(int nb) {
		valeurPotion = nb;
	}

	// ----------- Méthodes -----------

	// Ajoute une potion dans l'inventaire
	public void ajouterPotion() {
		System.out.println("");
		System.out.println("Vous avez trouvé une potion de soin (+" + valeurPotion + ") !");
		nbPotion++;
	}
}
