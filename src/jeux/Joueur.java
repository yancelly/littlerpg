/* LittleRPG - Jeu d'aventure où le joueur doit monter des étages.
 * 
 * Copyright (C) 2019  Yoan ANCELLY

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. 
    
*/

/* La classe Joueur dépend de Personnage et permet de créer un joueur avec ses caractéristiques spécifiques.
 * 
 * Les actions que seul le joueur peut effectuer se trouvent ici.
 * 
 *  */

package jeux;

public class Joueur extends Personnage {

	// Attributs
	private int experience, vie;


	// Constructeur
	public Joueur(String n) {
		super(n);
		vie = 3;
		experience = 0;
		setSante(getSanteMax());
	}

	// ----------- Accesseurs -----------
	public int getExperience() {
		return experience;
	}
	
	public int getVie() {
		return vie;
	}
	
	// ----------- Modulateurs -----------
	public void setVie(int v) {
		vie = v;
	}
	
	public void setExperience(int xp) {
		experience = xp;
	}

	// ----------- Méthodes -----------
	
	// Repasse le joueur en stats de base
	public void resetStats() {
		setNiveau(1);
		setSanteMax(12);
		setSante(getSanteMax());
		setEstVivant(true);
		experience = 0;
		vie = 3;
	}

	// Le joueur monte d'un niveau
	public void gagnerExperience() {
		experience += (int) (Math.random() * 11 + 15); // Gagne entre 15 et 25 points d'experience
		
		// Si XP = 100 le joueur monte de niveau
		if(experience >= 100) {
			setNiveau(getNiveau()+1); // Niveau +1
			setSanteMax(getSanteMax()+2); // Sante +2
			setSante(getSanteMax());
			experience = 0; // Reset XP
			System.out.println("");
			System.out.println("Félicitations " + getNomPersonnage() + " est monté(e) au niveau " + getNiveau()); // Annonce montée de niveau
		} else {
			System.out.println("");
			System.out.println("Experience : " + experience + "/100"); // Affiche l'XP gagné
		}
	}

	// Le joueur boit une potion de soin
	
	public String boirePotion(Potion p) {
		// Si le joueur n'a pas de potion	
		if (p.getNbPotion() <= 0) {
			System.out.println("");
			return "Vous n'avez plus de potions !";
		}	
		// Si le joueur a des potions mais que sa Sante n'est pas pleine		
		else if (getSante() < getSanteMax() && p.getNbPotion() > 0) {
			p.setNbPotion(p.getNbPotion() - 1); // Enlève une potion dans l'inventaire
			setSante(getSante() + p.getValeurPotion()); // Ajoute la valeur de la potion à la Sante du joueur
			
			// Si la Sante dépasse la Sante MAX
			if(getSante() > getSanteMax()) {
				setSante(getSanteMax()); // Replace la Sante à Sante MAX
			}
			System.out.println("");
			return "Potion consommée. " + p.getNbPotion() + " restant(s)";
		} 	
		// Sinon affiche
		else {
			System.out.println("");
			return "Santé pleine !";
		}
		
	}

	// Affiche les stats du personnage
	public void afficherStatPersonnage() {
		System.out.println(""); // Saut de ligne
		System.out.println("------------- Stats Personnage -------------");
		System.out.println("Nom : " + getNomPersonnage());
		System.out.println("Santé : " + getSante() + "/" + getSanteMax());
		System.out.println("Niveau : " + getNiveau());
		System.out.println("Experience : " + experience + "/100");
		System.out.println("Vie : " + vie + "/3");
		System.out.println("--------------------------------------------");
		System.out.println(""); // Saut de ligne
	}



}
