/* LittleRPG - Jeu d'aventure où le joueur doit monter des étages.
 * 
 * Copyright (C) 2019  Yoan ANCELLY

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. 

 */

package jeux;

public class Armure {

	// ----------- Attributs -----------
	private int defense, defenseMin, defenseMax, rangeDefense;
	private String nomArmure;
	private String nomAttribut, nomAttribut1, nomAttribut2, descriptionAttribut,descriptionAttribut1, descriptionAttribut2;
	private boolean epineuse, renforcee, estLegendaire;

	// ----------- Constructeurs -----------
	public Armure() {
		nomArmure = "Armure d'entraînement";
		defenseMin = 1;
		defenseMax = 3;
		rangeDefense = (defenseMax - defenseMin) + 1;
		nomAttribut = nomAttribut1 = nomAttribut2 = "";
		estLegendaire = false;
		descriptionAttribut = descriptionAttribut1 = descriptionAttribut2 = "Aucun";
		epineuse = renforcee = false;
	}

	// ----------- Accesseurs -----------
	public int getDefense() {
		return defense;
	}

	public int getDefenseMax() {
		return defenseMax;
	}

	public int getDefenseMin() {
		return defenseMin;
	}

	public int getRangeDefense() {
		return rangeDefense;
	}

	public String getNomArmure() {
		return nomArmure;
	}

	public boolean getEpineuse() {
		return epineuse;
	}

	public boolean getRenforcee() {
		return renforcee;
	}

	public boolean getEstLegendaire() {
		return estLegendaire;
	}

	// ----------- Modulateurs -----------
	public void setNomArmure(String n) {
		nomArmure = n;
	}

	public void setDefenseMax(int d) {
		defenseMax = d;
	}

	public void setDefenseMin(int d) {
		defenseMin = d;
	}

	public void setDefense(int d) {
		defense = d;
	}

	public void setEstLegendaire(boolean etat) {
		estLegendaire = etat;
	}

	// ----------- Méthodes -----------

	// Liste des attributs
	public void choixAttributsArmure(Joueur joueur) {
		int nbAttribut = 2;
		int choixAttr1 = 0;
		int choixAttr2 = 0;

		choixAttr1 = (int) (Math.random() * nbAttribut + 1);

		// Attribut 1

		if(choixAttr1 == 1) {
			attrEpineuse();
			nomAttribut1 = nomAttribut;
			descriptionAttribut1 = descriptionAttribut;
		}
		if(choixAttr1 == 2) {
			attrRenforcee();
			nomAttribut1 = nomAttribut;
			descriptionAttribut1 = descriptionAttribut;
		}

		// Attribut 2
		
		if(estLegendaire == true) {
			do {
				choixAttr2 = (int) (Math.random() * nbAttribut + 1);
			} while (choixAttr1 == choixAttr2);
			
			if(choixAttr2 == 1) {
				attrEpineuse();
				nomAttribut2 = nomAttribut;
				descriptionAttribut2 = descriptionAttribut;
			}
			if(choixAttr2 == 2) {
				attrRenforcee();
				nomAttribut2 = nomAttribut;
				descriptionAttribut2 = descriptionAttribut;
			}
		}
	}

	// Mettre les attributs à False
	public void resetAttributs() {
		epineuse = renforcee = false;
		nomAttribut = nomAttribut1 = nomAttribut2 = "";
		descriptionAttribut = descriptionAttribut1 = descriptionAttribut2 = "Aucun";
	}
	
	// Attribut Renforcee
	public void attrRenforcee() {
		renforcee = true;
		nomAttribut = "Renforcée";
		descriptionAttribut = "Augmente la défense d'un montant égale à la moitié du niveau du joueur";
	}
	
	// Attribut Epineuse
	public void attrEpineuse() {
		epineuse = true;
		nomAttribut = "Épineuse";
		descriptionAttribut = "Renvoie 50% des dégâts subis à l'ennemi";
	}

	// Affiche les statistiques de l'Armure
	public void afficherStatsArmure() {
		System.out.println(""); // Saut de ligne
		System.out.println("---------------- Stats Armure ----------------");
		System.out.println("Nom : " + nomArmure + " " + nomAttribut1 + " " + nomAttribut2);
		System.out.println("Défense : " + (defenseMax+defenseMin)/2 + " [" + defenseMin + "-" + defenseMax + "]");
		System.out.println("Attribut 1 : " + descriptionAttribut1);
		System.out.println("Attribut 2 : " + descriptionAttribut2);
		System.out.println("----------------------------------------------");
		System.out.println(""); // Saut de ligne
	}

	// Échanger son Armure actuelle contre l'Armure proposée
	public void echangerArmure(Armure armureDrop) {
		// Reset les attributs
		resetAttributs();
		// Attributs de l'armure
		nomArmure = armureDrop.getNomArmure();
		defenseMin = armureDrop.getDefenseMin();
		defenseMax = armureDrop.getDefenseMax();
		nomAttribut1 = armureDrop.nomAttribut1;
		nomAttribut2 = armureDrop.nomAttribut2;
		descriptionAttribut1 = armureDrop.descriptionAttribut1;
		descriptionAttribut2 = armureDrop.descriptionAttribut2;
		// Attributs spéciaux
		epineuse = armureDrop.epineuse;
		renforcee = armureDrop.renforcee;

	}

	// L'Armure devient de qualité Commun
	public void armureCommun(Joueur joueur) {
		nomArmure = "Armure Commun";
		defenseMin = 1 + joueur.getNiveau() - 1;
		defenseMax = 3 + joueur.getNiveau() - 1;	
		descriptionAttribut1 = descriptionAttribut2 = "Aucun";
		nomAttribut1 = nomAttribut2 = "";
	}

	// L'Armure devient de qualité Rare
	public void armureRare(Joueur joueur) {
		nomArmure = "Armure Rare";
		defenseMin = 3 + joueur.getNiveau() - 1;
		defenseMax = 5 + joueur.getNiveau() - 1;
		descriptionAttribut1 = descriptionAttribut2 = "Aucun";
		nomAttribut1 = nomAttribut2 = "";
	}

	// L'Armure devient de qualité Épique
	public void armureEpique(Joueur joueur) {
		nomArmure = "Armure Épique";
		defenseMin = 5 + joueur.getNiveau() - 1;
		defenseMax = 7 + joueur.getNiveau() - 1;
	}

	// L'Armure devient de qualité Légendaire
	public void armureLegendaire(Joueur joueur) {
		nomArmure = "Armure Légendaire";
		defenseMin = 7 + joueur.getNiveau() - 1;
		defenseMax = 9 + joueur.getNiveau() - 1;
	}
}
