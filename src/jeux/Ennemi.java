/* LittleRPG - Jeu d'aventure où le joueur doit monter des étages.
 * 
 * Copyright (C) 2019  Yoan ANCELLY

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
     
*/    

/* La classe Ennemi dépend de Personnage et regroupe les différents noms disponibles pour les ennemis */

package jeux;

public class Ennemi extends Personnage {
	
	static // Attributs
	String[] tabNomEnnemi={"Ceres",
			"Copper",
			"Demeter",
			"Earthea",
			"Fichtelite",
			"Gaia",
			"Haniyas",
			"Jarn",
			"Lando",
			"Laterite",
			"Maa",
			"Madd",
			"Mu",
			"Nog",
			"Nova",
			"Reki",
			"Sial",
			"Terremoto",
			"Topo",
			"Uralite",
			"Ye'elimite",
			"Ziemia"};

	// ----------- Constructeurs -----------
	
	public Ennemi() {
		super(tabNomEnnemi[(int)(Math.random()*tabNomEnnemi.length)]); // Choisit un nom (random)
	}
	
	// ----------- Méthodes -----------
	
	// Donne un nouveau nom à Ennemi
	public void changerNomEnnemi(Ennemi ennemi) {
		ennemi.setNom(tabNomEnnemi[(int)(Math.random()*tabNomEnnemi.length)]); // Change de nom (random)
	}
	
	public void resetStats() {
		setNiveau(1);
		setSanteMax(12);
		setSanteMin(8);
		nouvelleSante();
		setEstVivant(true);
	}


}
