/* LittleRPG - Jeu d'aventure où le joueur doit monter des étages.
 * 
 * Copyright (C) 2019  Yoan ANCELLY

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. 

 */

/* 
 * 
 * La classe Personnage regroupe toutes les caractéristiques de base d'un personnage (Joueur ou Ennemi)
 * 
 * Les actions que tous les personnages peuvent faire sont dans les méthodes
 * 
 * */

package jeux;

public class Personnage {

	// ----------- Attributs -----------
	private int sante, santeMax, santeMin, rangeSante, niveau;
	private boolean estVivant;
	private String nomPersonnage;

	// ----------- Constructeur -----------
	public Personnage(String n) {
		nomPersonnage = n;
		estVivant = true;
		santeMax = 12;
		santeMin = 8;
		rangeSante = (santeMax - santeMin) + 1;
		sante = (int) (Math.random() * rangeSante) + santeMin;
		niveau = 1;
	}


	// ----------- Accesseurs -----------
	public String getNomPersonnage() {
		return nomPersonnage;
	}

	public int getSante() {
		return sante;
	}

	public int getSanteMax() {
		return santeMax;
	}

	public int getSanteMin() {
		return santeMin;
	}

	public int getRangeSante() {
		return rangeSante;
	}

	public int getNiveau() {
		return niveau;
	}

	public boolean getEstVivant() {
		return estVivant;
	}

	// ----------- Modulateurs -----------
	public void setSante(int sante) {
		this.sante = sante;
	}

	public void setSanteMax(int santeMax) {
		this.santeMax = santeMax;
	}

	public void setSanteMin(int santeMin) {
		this.santeMin = santeMin;
	}

	public void setNom(String n) {
		nomPersonnage = n;
	}

	public void setEstVivant(boolean etat) {
		estVivant = etat;
	}

	public void setNiveau(int n) {
		niveau = n;
	}

	// ----------- Méthodes -----------

	// Permet au personnage d'attaquer une cible avec une Arme précise
	public void attaquer(Personnage cible, Arme arme, Armure armure) {
		arme.setDegats( (int) (Math.random() * arme.getRangeDegats() + arme.getDegatsMin()) ); // Dégâts (random) entre degatsMin et degatsMax
		armure.setDefense( (int) (Math.random() * armure.getRangeDefense() + armure.getDefenseMin()) );

		// Si la cible et l'attaquant sont vivant
		if(cible.estVivant = true && estVivant == true) {
			int degatsInflige = arme.getDegats() - armure.getDefense();

			// Attribut Maudit

			if(arme.getMaudit() == true) {
				degatsInflige += Math.round((degatsInflige * 20) /100);
				armure.setDefense(armure.getDefense() + ((degatsInflige * 35)/100)); 
			}

			// Attribut Renforcée
			if(armure.getRenforcee() == true) {
				degatsInflige -= Math.round(cible.niveau/2);
			}

			if(degatsInflige < 0) {
				degatsInflige = 0;
			}

			System.out.println("");
			System.out.println(nomPersonnage + " attaque " + cible.nomPersonnage + " et lui inflige " + degatsInflige + " points de dégâts.");
			cible.sante -= degatsInflige; // La cible pert de la sante selon les dégâts reçus

			//*** ARME ***//

			// Attribut Double Tranchant
			if(arme.getDoubleTranchant() == true) {
				System.out.println("Double Tranchant : " + nomPersonnage + " inflige " + Math.round((arme.getDegatsMax() * 33) / 100) + " points de dégâts supplémentaire.");
				cible.sante -= Math.round((arme.getDegatsMax() * 33) / 100);
				if(sante <= 0) {
					estVivant = false;
				} else {
					estVivant = true;
				}
			}

			// Attribut Sanguinaire
			if(arme.getSanguinaire() == true) {
				System.out.println("Sanguinaire : " + nomPersonnage + " regagne " + Math.round((santeMax * 35)/100) + " points de santé.");
				sante += Math.round((santeMax * 35) / 100);
				if(sante > santeMax) {
					sante = santeMax;
				}
			}

			//*** ARMURE ***//

			// Attribut Épineuse
			if(armure.getEpineuse() == true) {
				System.out.println("Épineuse : Renvoie " + Math.round(degatsInflige/2) + " points de dégâts subis à " + nomPersonnage);
				sante -= Math.round(degatsInflige/2);
				if(sante <= 0) {
					estVivant = false;
				} else {
					estVivant = true;
				}
			}

		}

		// Si la cible n'a plus de sante
		if(cible.sante <= 0) {
			cible.estVivant = false;
			cible.sante = 0; //
			System.out.println("");
			System.out.println(cible.nomPersonnage + " a été vaincu !");
			System.out.println("");
			System.out.println(nomPersonnage + " : " + sante + " points de santé restant(s).");
		} else { // Sinon affiche les points de sante
			System.out.println(cible.nomPersonnage + " : " + cible.sante + " points de santé restant(s).");
		}
	}

	// Recalcule la santé du personnage
	public int nouvelleSante() {
		sante = (int) (Math.random() * rangeSante) + santeMin;
		return sante;
	}
}
