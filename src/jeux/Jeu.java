/* LittleRPG - Jeu d'aventure où le joueur doit monter des étages.
 * 
 * Copyright (C) 2019  Yoan ANCELLY

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. 

 */

/* 
 * LittleRPG est un jeu de surSante où l'on incarne un personnage qui a pour objectif de monter le plus haut possible dans la Tour des Ombres.
 * 
 * Lorsque le joueur tue un monstre, il monte un étage et gagne de l'expérience. 
 * Le joueur peut s'équiper d'une arme, d'une armure et peut utiliser des potions.
 * 
 * Tous les 10 étages, il affronte un boss qui donne de meilleurs équipements.
 * Tous les 50 étages, il affronte un dieu qui donne un titre et obligatoirement des objets uniques.
 * 
 * S'il le joueur meurt, il recommence tout à zéro.
 * 
 */

package jeux;

import java.util.Scanner;

public class Jeu {
	
	static int etage;

	@SuppressWarnings("resource")
	public static void main(String[] args) {

	    int tour, etageMax, compteurPoison, compteurEtourdissement;
		double chance;
		String choix;
		int nbArmeCommun, nbArmeRare, nbArmeEpique, nbArmeLegendaire;
		int nbArmureCommun, nbArmureRare, nbArmureEpique, nbArmureLegendaire;

		choix = "0";
		tour = 1;
		etage = 1;
		etageMax = 1;
		compteurPoison = 0;
		compteurEtourdissement = 0;

		nbArmeCommun = 1;
		nbArmeRare = 0;
		nbArmeEpique = 0;
		nbArmeLegendaire = 0;

		nbArmureCommun = 1;
		nbArmureRare = 0;
		nbArmureEpique = 0;
		nbArmureLegendaire = 0;

		/************* DEBUT Initialisation Objets ***************/

		Scanner user_input = new Scanner(System.in);

		Potion potion;
		potion = new Potion();

		Joueur joueur;
		joueur = new Joueur("");

		Ennemi ennemi;
		ennemi = new Ennemi();

		Arme armeJoueur, armeEnnemi, armeDrop;
		armeJoueur = new Arme();
		armeEnnemi = new Arme();
		armeDrop = new Arme();
		
		Armure armureJoueur, armureEnnemi, armureDrop;
		armureJoueur = new Armure();
		armureEnnemi = new Armure();
		armureDrop = new Armure();

		/************* FIN Initialisation des objets ***************/


		/************* DEBUT Trame du jeu ***************/

		System.out.println("|---------------------------------------|");
		System.out.println("|                                       |");
		System.out.println("|               LittleRPG               |");
		System.out.println("|                                       |");
		System.out.println("|---------------------------------------|");

		// Attendre 500ms
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("");

		System.out.println("Bienvenue voyageur sur LittleRPG");
		System.out.print("Entrez votre nom : ");
		joueur.setNom(user_input.next());

		System.out.println("");
		System.out.println("Heureux de vous rencontrer " + joueur.getNomPersonnage());
		System.out.println("Vous vous trouvez à l'étage " + etage + " de la Tour des Ombres !");
		System.out.println("Bonne chance pour la suite de votre aventure !");

		// Attendre 1 secondes
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// Affichage des statistiques du joueur
		System.out.println("");
		System.out.println("Voici vos statistiques :");
		joueur.afficherStatPersonnage();
		armeJoueur.afficherStatsArme();
		armureJoueur.afficherStatsArmure();

		// Attendre 2 secondes
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("********************* Étage " + etage + " *********************");
		System.out.println("");
		System.out.println("Attention ! " + ennemi.getNomPersonnage() + " (" + ennemi.getSante() + " points de santé) approche !");

		/************* FIN Trame du jeu ***************/

		/**********************************************/

		while(true) {

			System.out.println("");
			System.out.println("******************** Tour " + tour + " ********************");
			tour++;

			// Choix du joueur

			System.out.println("");
			System.out.println("1 - Voir les statistiques du personnage");
			System.out.println("2 - Attaquer la cible");
			System.out.println("3 - Boire une potion ( " + potion.getNbPotion() + "/" + potion.getNbPotionLimite() + " restant(s) )");
			System.out.println("4 - Prendre la fuite (-3 étages)");
			System.out.println("");

			System.out.print("Que voulez-vous faire ? : ");

			choix = user_input.next();

			// Voir les stats
			if(choix.equals("1")) {
				joueur.afficherStatPersonnage(); // Affiche les stats du joueur
				armeJoueur.afficherStatsArme(); // Affiche les stats de l'arme du joueur
				armureJoueur.afficherStatsArmure(); // Affiche les stats de l'armure du joueur
				tour--;
				// Attendre 1 secondes
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			// Attaquer l'ennemi
			else if(choix.equals("2")) {
				joueur.attaquer(ennemi, armeJoueur, armureEnnemi); // Le joueur attaque l'ennemi

				if(ennemi.getEstVivant() == true) {

					// Attribut Poison (35% de chance)
					if(armeJoueur.getPoison() == true) {
						// Initialisation compteurPoison
						if(compteurPoison == 0) {
							chance = (int) (Math.random() * 100 + 1);
							if(chance <= 35) {
								compteurPoison = 2;
							}
						}
						// Active le poison
						if(compteurPoison > 0) {
							System.out.println("");
							System.out.println("L'ennemi est empoisonné pendant " + compteurPoison + " tour(s) !");
							System.out.println("Vous lui infligez " + armeJoueur.getDegatsMax()/2 + " points de dégâts supplémentaire.");
							ennemi.setSante(ennemi.getSante() - armeJoueur.getDegatsMax()/2);
							compteurPoison--;
							if(ennemi.getSante() <= 0) {
								ennemi.setEstVivant(false);
							}
						}
						
						// Etourdissement désactivé
						if(armeJoueur.getEtourdissement() == false) {
							ennemi.attaquer(joueur, armeEnnemi, armureJoueur);
						}

					}

					// Attribut Etourdissement (20% de chance)
					if(armeJoueur.getEtourdissement() == true) {
						// Initialisation compteurEtourdissement
						if(compteurEtourdissement == 0) {
							chance = (int) (Math.random() * 100 + 1);
							if(chance <= 20) {
								compteurEtourdissement = 2;
								// Etourdi l'ennemi
								if(compteurEtourdissement > 0) {
									System.out.println("");
									System.out.println("L'ennemi est étourdi ! Il ne plus attaquer pendant " + compteurEtourdissement + " tour(s)");
									compteurEtourdissement--;
								}
							} else { // Etourdissement désactivé
								ennemi.attaquer(joueur, armeEnnemi, armureJoueur);
							} 
						}

					} else { // Poison et Etourdissement désactivé
						ennemi.attaquer(joueur, armeEnnemi, armureJoueur);
					}

				}
			}
			
			// Boire une potion
			else if(choix.equals("3")) {
				System.out.println(joueur.boirePotion(potion)); // Le joueur boit une potion

				// Attribut Poison (35% de chance)
				if(armeJoueur.getPoison() == true) {
					// Active le poison
					if(compteurPoison > 0) {
						System.out.println("");
						System.out.println("L'ennemi est empoisonné pendant " + compteurPoison + " tour(s) !");
						System.out.println("Vous lui infligez " + armeJoueur.getDegatsMax()/2 + " points de dégâts supplémentaire.");
						ennemi.setSante(ennemi.getSante() - armeJoueur.getDegatsMax()/2);
						compteurPoison--;
						if(ennemi.getSante() <= 0) {
							ennemi.setEstVivant(false);
						}
					}

				}

				// Attribut Etourdissement (20% de chance)
				if(armeJoueur.getEtourdissement() == true) {
					// Etourdi l'ennemi
					if(compteurEtourdissement > 0) {
						System.out.println("");
						System.out.println("L'ennemi est étourdi ! Il ne plus attaquer pendant " + compteurEtourdissement + " tour(s)");
						compteurEtourdissement--;
					} else {
						ennemi.attaquer(joueur, armeEnnemi, armureJoueur);
					}

				} else {
					ennemi.attaquer(joueur, armeEnnemi, armureJoueur);
				}
			}
			
			// Prendre la fuite
			else if(choix.equals("4")) {
				if(etage > 3) {
					tour = 1;
					compteurEtourdissement = 0;
					compteurPoison = 0;
					etage -= 3; // Descendre 3 étages
					System.out.println("Vous fuyez et descendez 3 étages plus bas !");
					apparitionEnnemi(ennemi, armeEnnemi, armureEnnemi, joueur);
				} else {
					tour--;
					System.out.println("Vous ne pouvez pas descendre plus bas !");
				}
			}
			else {
				tour--;
				System.out.println("Entrez un choix correct !"); // Choix non valide
			}

			// Fix Sante joueur
			if(joueur.getSante() > 0) {
				joueur.setEstVivant(true);
			} else {
				joueur.setEstVivant(false);
			}

			// Si le joueur est mort

			if(joueur.getEstVivant() == false) {

				if(etage%10 == 0 || etage%5 == 0) {
					// Rester au même niveau
					joueur.setVie(joueur.getVie()-1); // Le joueur perd une vie
				} else {
					if(etage >= 2) {
						etage--; // Descend d'un étage
					}
					System.out.println("");
					System.out.println("Vous êtes mort ! Vous descendez d'un étage et perdez une vie !");
					joueur.setVie(joueur.getVie()-1); // Le joueur perd une vie
				}

				System.out.println("");
				System.out.println(joueur.getVie() + " vie(s) restant(s)");
				tour = 1;
				joueur.setSante(joueur.getSanteMax()); // Le joueur retrouve sa santé

				// Si le joueur n'a plus de vie
				if(joueur.getVie() == 0) {
					// Reset les stats du joueur
					joueur.resetStats();
					// Arme Joueur
					armeJoueur.setEstLegendaire(false);
					armeJoueur.resetAttributs();
					armeJoueur.armeCommun(joueur);
					// Armure Joueur
					armureJoueur.setEstLegendaire(false);
					armureJoueur.resetAttributs();
					armureJoueur.armureCommun(joueur);
					// Reset les stats de l'ennemi
					ennemi.resetStats();
					// Arme Ennemi
					armeEnnemi.setEstLegendaire(false);
					armeEnnemi.resetAttributs();
					armeEnnemi.armeCommun(joueur);
					// Armure Ennemi
					armureEnnemi.setEstLegendaire(false);
					armureEnnemi.resetAttributs();
					armureEnnemi.armureCommun(joueur);
					// Texte de fin
					System.out.println("");
					System.out.println("Vous n'avez plus de vie ! Toutes vos statistiques et vos objets sont perdus !");
					System.out.println("");
					System.out.println("Étage maximum atteint : " + etageMax);
					System.out.println("");
					System.out.println("********************* FIN DE LA PARTIE *********************");
					System.out.println("");
					System.out.println("");
					System.out.println("****************** NOUVELLE DE LA PARTIE ******************");
					// Reset la partie
					potion.setNbPotion(2);
					potion.setValeurPotion(10);
					etage = 1;
					tour = 1;
					etageMax = 1;
					// Reset Armes
					nbArmeCommun = 1;
					nbArmeRare = 0;
					nbArmeEpique = 0;
					nbArmeLegendaire = 0;
					// Reset Armures
					nbArmureCommun = 1;
					nbArmureRare = 0;
					nbArmureEpique = 0;
					nbArmureLegendaire = 0;
				}

				System.out.println("");
				System.out.println("********************* Étage " + etage + " *********************");

				joueur.setEstVivant(true);
			}

			// Si l'ennemi est mort ET le joueur est vivant

			if(ennemi.getEstVivant() == false && joueur.getEstVivant() == true) {
				if(etage%10 == 0) {
					joueur.setExperience(joueur.getExperience()+10);
				} else if (etage%5 == 0) {
					joueur.setExperience(joueur.getExperience()+5);
					potion.setValeurPotion(potion.getValeurPotion()+2);
				}
				joueur.gagnerExperience(); // Gagne de l'experience
				tour = 1; // Reset le nombre de tour

				// ---------------- Drop Potion -----------------

				// Drop Potion (50% de chance de drop)
				chance = (int) (Math.random() * 100 + 1);
				if(chance <= 50) {
					if(potion.getNbPotion() < potion.getNbPotionLimite()) {
						potion.ajouterPotion();
						System.out.println("Votre santé : " + joueur.getSante() + "/" + joueur.getSanteMax());
						System.out.println("1 - Consommer la potion");
						System.out.println("2 - Mettre dans l'inventaire");
						System.out.print("Que voulez-vous faire ? : ");
						choix = user_input.next();
						if(Integer.parseInt(choix) == 1) {
							joueur.boirePotion(potion);
						} else if(Integer.parseInt(choix) == 2) {
							// Ne rien faire - Passer à la suite
						} else {
							System.out.println("Entrez un choix correct !");
						}
					} else {
						System.out.println("Votre nombre de potion est au maximum !");
					}
				}

				// ---------------- Drop Arme -----------------

				/********** DROP ARME DIVIN ***********/

				// Drop Arme Légendaire (8% de chance de drop sur le Boss)
				if(etage%10 == 0) {
					chance = (int) (Math.random() * 100 + 1);
					armeDrop.setEstLegendaire(true);
					armeDrop.resetAttributs();
					armeDrop.choixAttributsArme(joueur);
					armeDrop.armeLegendaire(joueur);
					if(chance <= 8) {
						System.out.println("");
						System.out.println("Vous avez trouvé une arme Légendaire !");
						System.out.println("");
						System.out.println("--------------- Arme Actuelle ---------------");
						armeJoueur.afficherStatsArme();
						System.out.println("--------------- Nouvelle Arme ---------------");
						armeDrop.afficherStatsArme();
						afficherChoixArme(armeJoueur, armeDrop); // Demande au joueur s'il veut changer d'arme
						nbArmeLegendaire++;
						System.out.println("");
						System.out.println(nbArmeLegendaire + " arme Légendaire dans l'inventaire.");
					}
				}

				// Drop Arme Épique (17% de chance de drop sur le Mini-boss)
				if(etage%5 == 0) {
					chance = (int) (Math.random() * 100 + 1);
					armeDrop.setEstLegendaire(false);
					armeDrop.resetAttributs();
					armeDrop.choixAttributsArme(joueur);
					armeDrop.armeEpique(joueur);
					if(chance <= 17) {
						System.out.println("");
						System.out.println("Vous avez trouvé une arme Épique !");
						System.out.println("");
						System.out.println("--------------- Arme Actuelle ---------------");
						armeJoueur.afficherStatsArme();
						System.out.println("--------------- Nouvelle Arme ---------------");
						armeDrop.afficherStatsArme();
						afficherChoixArme(armeJoueur, armeDrop);
						nbArmeEpique++;
						System.out.println("");
						System.out.println(nbArmeEpique + " arme Épique dans l'inventaire.");
					}
				}

				// Drop Arme Rare (25% de chance de drop sur un ennemi normal)
				chance = (int) (Math.random() * 100 + 1);
				armeDrop.setEstLegendaire(false);
				armeDrop.resetAttributs();
				armeDrop.armeRare(joueur);
				if(chance <= 25 && armeJoueur.getDegatsMax() < armeDrop.getDegatsMax()) {
					System.out.println("");
					System.out.println("Vous avez trouvé une arme Rare !");
					System.out.println("");
					System.out.println("--------------- Arme Actuelle ---------------");
					armeJoueur.afficherStatsArme();
					System.out.println("--------------- Nouvelle Arme ---------------");
					armeDrop.afficherStatsArme();
					afficherChoixArme(armeJoueur, armeDrop);
					nbArmeRare++;
					System.out.println(nbArmeRare + " arme Rare dans l'inventaire.");
				}else if(chance <= 25){
					nbArmeRare++;
					System.out.println("");
					System.out.println("Vous avez trouvé une arme Rare ! " + nbArmeRare + " dans l'inventaire.");
				}

				// Drop Arme Commun (50% de chance de drop sur un ennemi normal)
				chance = (int) (Math.random() * 100 + 1);
				armeDrop.setEstLegendaire(false);;
				armeDrop.resetAttributs();
				armeDrop.armeCommun(joueur);
				if(chance <= 50 && armeJoueur.getDegatsMax() < armeDrop.getDegatsMax()) {
					System.out.println("");
					System.out.println("Vous avez trouvé une arme Commune !");
					System.out.println("");
					System.out.println("--------------- Arme Actuelle ---------------");
					armeJoueur.afficherStatsArme();
					System.out.println("--------------- Nouvelle Arme ---------------");
					armeDrop.afficherStatsArme();
					afficherChoixArme(armeJoueur, armeDrop);
					nbArmeCommun++;
					System.out.println(nbArmeCommun + " arme Commune dans l'inventaire.");
				}else if(chance <= 50) {
					nbArmeCommun++;
					System.out.println("");
					System.out.println("Vous avez trouvé une arme Commune ! " + nbArmeCommun + " dans l'inventaire.");
				}

				// ---------------- Drop Armure -----------------

				/********** DROP ARMURE DIVIN ***********/

				// Drop Armure Légendaire (8% de chance de drop sur le Boss)
				if(etage%10 == 0) {
					chance = (int) (Math.random() * 100 + 1);
					armureDrop.setEstLegendaire(true);
					armureDrop.resetAttributs();
					armureDrop.choixAttributsArmure(joueur);
					armureDrop.armureLegendaire(joueur);
					if(chance <= 8) {
						System.out.println("");
						System.out.println("Vous avez trouvé une armure Légendaire !");
						System.out.println("");
						System.out.println("--------------- Armure Actuelle ---------------");
						armureJoueur.afficherStatsArmure();
						System.out.println("--------------- Nouvelle Armure ---------------");
						armureDrop.afficherStatsArmure();
						afficherChoixArmure(armureJoueur, armureDrop); // Demande au joueur s'il veut changer d'armure
						nbArmureLegendaire++;
						System.out.println("");
						System.out.println(nbArmureLegendaire + " armure Légendaire dans l'inventaire.");
					}
				}
				// Drop Armure Épique (17% de chance de drop sur le Mini-boss)
				if(etage%5 == 0) {
					chance = (int) (Math.random() * 100 + 1);
					armureDrop.setEstLegendaire(false);
					armureDrop.resetAttributs();
					armureDrop.choixAttributsArmure(joueur);
					armureDrop.armureEpique(joueur);
					if(chance <= 17) {
						System.out.println("");
						System.out.println("Vous avez trouvé une armure Épique !");
						System.out.println("");
						System.out.println("--------------- Armure Actuelle ---------------");
						armureJoueur.afficherStatsArmure();
						System.out.println("--------------- Nouvelle Armure ---------------");
						armureDrop.afficherStatsArmure();
						afficherChoixArmure(armureJoueur, armureDrop);
						nbArmureEpique++;
						System.out.println("");
						System.out.println(nbArmureEpique + " armure Épique dans l'inventaire.");
					}
				}
				// Drop Armure Rare (25% de chance de drop sur un ennemi normal)
				chance = (int) (Math.random() * 100 + 1);
				armureDrop.setEstLegendaire(false);
				armureDrop.resetAttributs();
				armureDrop.armureRare(joueur);
				if(chance <= 25 && armureJoueur.getDefenseMax() < armureDrop.getDefenseMax()) {
					System.out.println("");
					System.out.println("Vous avez trouvé une armure Rare !");
					System.out.println("");
					System.out.println("--------------- Armure Actuelle ---------------");
					armureJoueur.afficherStatsArmure();
					System.out.println("--------------- Nouvelle Armure ---------------");
					armureDrop.afficherStatsArmure();
					afficherChoixArmure(armureJoueur, armureDrop);
					nbArmureRare++;System.out.println("");
					System.out.println(nbArmureRare + " armure Rare dans l'inventaire.");
				}
				else if(chance <= 25){
					nbArmureRare++;
					System.out.println("");
					System.out.println("Vous avez trouvé une armure Rare ! " + nbArmureRare + " dans l'inventaire.");
				}

				// Drop Armure Commun (50% de chance de drop sur un ennemi normal)
				chance = (int) (Math.random() * 100 + 1);
				armureDrop.setEstLegendaire(false);
				armureDrop.resetAttributs();
				armureDrop.armureCommun(joueur);
				if(chance <= 50 && armureJoueur.getDefenseMax() < armureDrop.getDefenseMax()) {
					System.out.println("");
					System.out.println("Vous avez trouvé une armure Commune !");
					System.out.println("");
					System.out.println("--------------- Armure Actuelle ---------------");
					armureJoueur.afficherStatsArmure();
					System.out.println("--------------- Nouvelle Armure ---------------");
					armureDrop.afficherStatsArmure();
					afficherChoixArmure(armureJoueur, armureDrop);
					nbArmureCommun++;
					System.out.println("");
					System.out.println(nbArmureCommun + " armure Commune dans l'inventaire.");
				}
				else if(chance <= 50){
					nbArmureCommun++;
					System.out.println("");
					System.out.println("Vous avez trouvé une armure Commune ! " + nbArmureCommun + " dans l'inventaire.");
				}

				// ------------------- Fusion Arme -------------------

				// Si le nombre d'arme Commun est égale à 3, le joueur obtient une arme Rare
				if(nbArmeCommun == 3) {
					nbArmeCommun = 0;
					nbArmeRare++;
					armeDrop.setEstLegendaire(false);
					armeDrop.resetAttributs();
					armeDrop.armeRare(joueur);
					if(armeJoueur.getDegatsMax() < armeDrop.getDegatsMax()) {
						System.out.println("");
						System.out.println("Vous avez 3 armes Commune. La fusion commence !");
						System.out.println("Vous avez obtenu une arme Rare !");
						System.out.println("");
						System.out.println("--------------- Arme Actuelle ---------------");
						armeJoueur.afficherStatsArme();
						System.out.println("--------------- Nouvelle Arme ---------------");
						armeDrop.afficherStatsArme();
						afficherChoixArme(armeJoueur, armeDrop);
					}
				}

				// Si le nombre d'arme Rare est égale à 5, le joueur obtient une arme Épique
				if(nbArmeRare == 5) {
					nbArmeRare = 0;
					nbArmeEpique++;
					System.out.println("");
					System.out.println("Vous avez 5 armes Rare. La fusion commence !");
					System.out.println("Vous avez obtenu une arme Épique !");
					armeDrop.setEstLegendaire(false);
					armeDrop.resetAttributs();
					armeDrop.choixAttributsArme(joueur);
					armeDrop.armeEpique(joueur);
					System.out.println("");
					System.out.println("--------------- Arme Actuelle ---------------");
					armeJoueur.afficherStatsArme();
					System.out.println("--------------- Nouvelle Arme ---------------");
					armeDrop.afficherStatsArme();
					afficherChoixArme(armeJoueur, armeDrop);
				}

				// Si le nombre d'arme Épique est égale à 10, le joueur obtient une arme Légendaire
				if(nbArmeEpique == 10) {
					nbArmeEpique = 0;
					nbArmeLegendaire++;
					System.out.println("");
					System.out.println("Vous avez 10 armes Épique. La fusion commence !");
					System.out.println("Vous avez obtenu une arme Légendaire !");
					armeDrop.setEstLegendaire(true);
					armeDrop.resetAttributs();
					armeDrop.choixAttributsArme(joueur);
					armeDrop.armeLegendaire(joueur);
					System.out.println("");
					System.out.println("--------------- Arme Actuelle ---------------");
					armeJoueur.afficherStatsArme();
					System.out.println("--------------- Nouvelle Arme ---------------");
					armeDrop.afficherStatsArme();
					afficherChoixArme(armeJoueur, armeDrop);
				}

				// ------------------- Fusion Armure -------------------

				// Fusion 3 armures Commun = 1 Rare
				if(nbArmureCommun == 3) {
					nbArmureCommun = 0;
					nbArmureRare++;
					System.out.println("");
					System.out.println("Vous avez 3 armures Commune. La fusion commence !");
					System.out.println("Vous avez obtenu une armure Rare !");
					armureDrop.setEstLegendaire(false);
					armureDrop.resetAttributs();
					armureDrop.armureRare(joueur);
					if(armureJoueur.getDefenseMax() < armureDrop.getDefenseMax()) {
						System.out.println("");
						System.out.println("--------------- Armure Actuelle ---------------");
						armureJoueur.afficherStatsArmure();
						System.out.println("--------------- Nouvelle Armure ---------------");
						armureDrop.afficherStatsArmure();
						afficherChoixArmure(armureJoueur, armureDrop);
					}
				}

				// Fusion 5 armures Rare = 1 Épique
				if(nbArmureRare == 5) {
					nbArmureRare = 0;
					nbArmureEpique++;
					System.out.println("");
					System.out.println("Vous avez 5 armures Rare. La fusion commence !");
					System.out.println("Vous avez obtenu une armure Épique !");
					armureDrop.setEstLegendaire(false);
					armureDrop.resetAttributs();
					armureDrop.choixAttributsArmure(joueur);
					armureDrop.armureEpique(joueur);
					System.out.println("");
					System.out.println("--------------- Armure Actuelle ---------------");
					armureJoueur.afficherStatsArmure();
					System.out.println("--------------- Nouvelle Armure ---------------");
					armureDrop.afficherStatsArmure();
					afficherChoixArmure(armureJoueur, armureDrop);
				}

				// Fusion 10 armures Rare = 1 Légendaire
				if(nbArmureEpique == 10) {
					nbArmureEpique = 0;
					nbArmureLegendaire++;
					System.out.println("");
					System.out.println("Vous avez 10 armures Épique. La fusion commence !");
					System.out.println("Vous avez obtenu une armure Légendaire !");
					armureDrop.setEstLegendaire(true);
					armureDrop.resetAttributs();
					armureDrop.choixAttributsArmure(joueur);
					armureDrop.armureLegendaire(joueur);
					System.out.println("");
					System.out.println("--------------- Armure Actuelle ---------------");
					armureJoueur.afficherStatsArmure();
					System.out.println("--------------- Nouvelle Armure ---------------");
					armureDrop.afficherStatsArmure();
					afficherChoixArmure(armureJoueur, armureDrop);			
				}

				etage++; // Monte d'un étage

				if(etage > etageMax) {
					if(etageMax%10 == 0) {
						ennemi.setSanteMax(ennemi.getSanteMax()+3);
						ennemi.setSanteMin(ennemi.getSanteMin()+3);
					} 
					else if(etageMax%5 == 0) {
						ennemi.setSanteMax(ennemi.getSanteMax()+2);
						ennemi.setSanteMin(ennemi.getSanteMin()+2);
					}
					etageMax = etage; // Réglage etageMax
				}

				// Si l'ennemi est mort
				if(ennemi.getEstVivant() == false) {
					ennemi.nouvelleSante();
					ennemi.changerNomEnnemi(ennemi);
					tour = 1;
					compteurEtourdissement = 0;
					compteurPoison = 0;
				}

				// Recommence le processus pour un nouvel ennemi si l'ancien est mort
				apparitionEnnemi(ennemi, armeEnnemi, armureEnnemi, joueur);

				ennemi.setEstVivant(true);
			}
		}


	} // FIN main

	// ----------- Méthodes -----------

	// Affiche le choix pour changer d'Arme
	public static void afficherChoixArme(Arme armeJoueur, Arme armeDrop) {
		@SuppressWarnings("resource")
		Scanner user_input = new Scanner(System.in);
		String choix;
		System.out.println("");
		System.out.println("1 - Équiper l'arme");
		System.out.println("2 - Mettre dans l'inventaire");
		System.out.println("");
		System.out.print("Que voulez-vous faire ? : ");
		choix = user_input.next();
		if(choix.equals("1")) {
			armeJoueur.resetAttributs();
			armeJoueur.echangerArme(armeDrop);
		} else if(choix.equals("2")) {
			// Ne rien faire - Passer à la suite
		} else {
			System.out.println("Entrez un choix correct !");
		}

	}

	public static void afficherChoixArmure(Armure armureJoueur, Armure armureDrop) {
		@SuppressWarnings("resource")
		Scanner user_input = new Scanner(System.in);
		String choix;
		System.out.println("");
		System.out.println("1 - Équiper l'armure");
		System.out.println("2 - Mettre dans l'inventaire");
		System.out.println("");
		System.out.print("Que voulez-vous faire ? :");
		choix = user_input.next();
		if(choix.equals("1")) {
			armureJoueur.resetAttributs();
			armureJoueur.echangerArmure(armureDrop);
		} else if(choix.equals("2")) {
			// Ne rien faire - Passer à la suite
		} else {
			System.out.println("Entrez un choix correct !");
		}

	}
	
	public static void apparitionEnnemi(Ennemi ennemi, Arme armeEnnemi, Armure armureEnnemi, Joueur joueur) {
		System.out.println("");
		System.out.println("********************* Étage " + etage + " *********************");
		System.out.println("");
		// Si l'étage est un multiple de 50, l'ennemi est un Dieu
		if(etage%50 == 0) {	
			ennemi.setSante(ennemi.getSanteMax() + joueur.getSanteMax() * 2);
			// Arme
			armeEnnemi.setEstLegendaire(true);
			armeEnnemi.resetAttributs();
			armeEnnemi.choixAttributsArme(joueur);
			armeEnnemi.armeLegendaire(joueur);
			// Armure
			armureEnnemi.setEstLegendaire(true);
			armureEnnemi.resetAttributs();
			armureEnnemi.choixAttributsArmure(joueur);
			armureEnnemi.armureLegendaire(joueur);
			System.out.println("Attention ! Dieu " + ennemi.getNomPersonnage() + " (" + ennemi.getSante() + " points de santé) approche !");
		}
		// Si l'étage est un multiple de 10, l'ennemi est un Boss
		else if(etage%10 == 0) {
			ennemi.setSante(ennemi.getSanteMax());
			// Arme
			armeEnnemi.setEstLegendaire(false);
			armeEnnemi.resetAttributs();
			armeEnnemi.choixAttributsArme(joueur);
			armeEnnemi.armeEpique(joueur);
			// Armure
			armureEnnemi.setEstLegendaire(false);
			armureEnnemi.resetAttributs();
			armureEnnemi.choixAttributsArmure(joueur);
			armureEnnemi.armureEpique(joueur);
			System.out.println("Attention ! Boss " + ennemi.getNomPersonnage() + " (" + ennemi.getSante() + " points de santé) approche !");
		}
		// Si l'étage est un multiple de 5, l'ennemi est un Mini-Boss
		else if(etage%5 == 0) {
			ennemi.setSante(ennemi.getSanteMax());
			// Arme
			armeEnnemi.setEstLegendaire(false);
			armeEnnemi.resetAttributs();
			armeEnnemi.armeRare(joueur);
			// Armure
			armureEnnemi.setEstLegendaire(false);
			armureEnnemi.resetAttributs();
			armureEnnemi.armureRare(joueur);
			System.out.println("Attention ! Mini-Boss " + ennemi.getNomPersonnage() + " (" + ennemi.getSante() + " points de santé) approche !");
		} else {
			System.out.println("Attention ! " + ennemi.getNomPersonnage() + " (" + ennemi.getSante() + " points de santé) approche !");
			// Arme
			armeEnnemi.setEstLegendaire(false);
			armeEnnemi.resetAttributs();
			armeEnnemi.armeCommun(joueur);
			// Armure
			armureEnnemi.setEstLegendaire(false);
			armureEnnemi.resetAttributs();
			armureEnnemi.armureCommun(joueur);
		}
	}

}
