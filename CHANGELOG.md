# CHANGELOG

### 12 février 2019 - v0.1.1

#### Ajout

* Affichage de la vie des personnages à la fin du combat

#### Changement

* La valeur des potions tous les 5 étages : <strike>+3</strike> => +2
* La défense de l'attribut Renforcée : <strike>égale au niveau du joueur</strike> => égale à la moitié du niveau du joueur

#### Patch

* Fonctionnement de l'attribut Étourdissement qui empêchait totalement l'ennemi d'attaquer.

***

### 11 février 2019 - v0.1.0

#### Ajout

* Initial Changelog : Tous les changements se trouveront ici maintenant